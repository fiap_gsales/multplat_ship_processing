int maxWidth = 800;

int xShip = 0;
int yShip = 20;
int speedShip = 5;
int widthShip = 100;
int heightShip = 20;
boolean hasShip = true;


int xSub = 50;
int speedSub = 40;


int yShoot = 280;
boolean shooted = false;

void setup(){
  size(maxWidth, 600);
}

void draw(){
  background(0);
  
  if(hasShip){
    ship();
  }
  
  submarine();
  
  if(shooted){
    shoot();
  }    
}

void ship() {
 
  rect(xShip, yShip, widthShip, heightShip);
  
  xShip = xShip + speedShip;
  
  if(xShip > maxWidth - (widthShip / 2)){
      speedShip *= -1;
  }
  
  if(xShip < 0){
     speedShip *= -1; 
  } 
  
}


void submarine(){
  ellipse(xSub, 300, 100, 20); 
}


void shoot(){
 int xShoot = xSub;
 rect(xShoot, yShoot, 5, 20);
 
 yShoot -= 10;
 
 boolean collidedX = xShoot >= xShip && xShoot <= (xShip + widthShip);
 boolean collidedY = yShoot <= heightShip; 
 
 if((collidedX && collidedY) || yShoot <= 0){
    yShoot = 280;
    shooted = false; 
 }
 
 if(collidedX && collidedY){
  hasShip = false; 
 }
}


void keyPressed(){
   if(key == CODED){
      if(keyCode == LEFT){
        if(xSub <= 60)
          return;
        
        xSub -= speedSub; 
      }
      else if(keyCode == RIGHT){
        if(xSub >= maxWidth - 60)
          return;
        
        xSub += speedSub;
      }
      else if(keyCode == UP){
         shooted = true; 
      }
   } 
}




